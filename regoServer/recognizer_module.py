import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision import *
import numpy as np
from PIL import Image
import torchvision.transforms.functional as TF
from torchvision.transforms import *
from learning.dataLoader import VideoFolder
import glob
import os
import cv2

class Recognizer():
  def __init__(self):
    self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    self._image_recognizer = models.resnet18()
    self._video_recognizer = models.video.r3d_18()

    self._image_recognizer = torch.load("resnet.pth", map_location="cuda:0")
    self._video_recognizer = torch.load("resnet3d.pth", map_location="cuda:0")

    self._image_recognizer.to(self.device)
    self._video_recognizer.to(self.device)

    self._image_recognizer.eval()
    self._video_recognizer.eval()

  def image_recognizer(self, path = "./toRecognize.jpg"):
    LABELS = {0:'A',1:'B', 2:'C', 3:'D', 4:'DEL', 5:'E', 6:'F', 7:'G', 8:'H', 9:'I', 10:'J', 11:'K', 12:'L', 13:'M', 14:'N', 15:'NOTHING',16:'O', 17:'P', 18:'Q',19:'R',20:'S',21:'SPACE',22:'T', 23:'U',24:'V', 25:'W',26:'X',27:'Y', 28:'Z'}
    image = Image.open(path)
    x = TF.to_tensor(image)
    x = TF.normalize(x,[0.485, 0.456, 0.406], [0.229, 0.224, 0.225] )
    x.unsqueeze_(0)
    with torch.no_grad():
      output = self._image_recognizer(x.cuda())
    return LABELS[int(torch.argmax(output))]

  def get_frame_names(self, path):
        frame_names = []
        for ext in ['.jpg', '.JPG', '.jpeg', '.JPEG']:
            frame_names.extend(glob.glob(os.path.join(path, "*" + ext)))
        frame_names = list(sorted(frame_names))
        num_frames = len(frame_names)

        num_frames_necessary = 16 * 1 * 2 
        offset = 0
        if num_frames_necessary > num_frames:
            frame_names += [frame_names[-1]] * (num_frames_necessary - num_frames)
        elif num_frames_necessary < num_frames:
            diff = (num_frames - num_frames_necessary)
            offset = np.random.randint(0, diff)
        frame_names = frame_names[offset:num_frames_necessary + 2]
        return frame_names

  def get_frames_from_video(self, path_to_video, path_to_frames):
    vidcap = cv2.VideoCapture(path_to_video)
    vidcap.set(cv2.CAP_PROP_FPS, 12)
    success,image = vidcap.read()
    count = 1
    fps_count = 0 
    while success:
      if fps_count % 2 == 0:
        cv2.imwrite(f"{path_to_frames}/{count}.jpg", cv2.resize(image, (128,100)))
        count += 1
      fps_count += 1
      success,image = vidcap.read()

  def video_recognizer(self, path = "./toRecognize.mp4"):
    files = glob.glob('toRecognize/*')
    for f in files:
      os.remove(f)
    self.get_frames_from_video(path, "toRecognize")
    
    img_paths = self.get_frame_names('toRecognize')
    LABELS = {0: 'No gesture', 1: 'Stop Sign', 2: 'Zooming In With Two Fingers', 3: 'Zooming Out With Two Fingers', 4: 'Thumb Up', 5: 'Thumb Down', 6: 'Swiping Left', 7: 'Swiping Right', 8: 'Sliding Two Fingers Down', 9: 'Sliding Two Fingers Up'}
    imgs = []
    for img_path in img_paths:
        img = Image.open(img_path).convert('RGB')
        transform = Compose([ CenterCrop(112), ToTensor(), Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]) ])
        img = transform(img)
        imgs.append(torch.unsqueeze(img, 0))
    x = torch.cat(imgs)
    x = x.permute(1, 0, 2, 3)
    x.unsqueeze_(0)
    with torch.no_grad():
      output = self._video_recognizer(x.cuda())
    return LABELS[int(torch.argmax(output))]
