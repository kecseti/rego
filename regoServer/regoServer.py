import flask
import os
import base64
from flask_cors import CORS
from recognizer_module import Recognizer
app = flask.Flask(__name__)
CORS(app)

UPLOAD_FOLDER = './'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

recognizer = Recognizer()

'''
    Simple endpoint that recognizes letters and SPACE and DELETE based on the American Sign Language Alphabet
    It uses POST method.
    the request body  with only one field, photo.
    Ex.
    {
      photo: image in base64
    }
    The given back response is a JSON object with the prediction
'''

@app.route("/asl/alphabet",  methods = ["POST"])
def asl_recognizer():
    try:
        file = base64.b64decode(flask.request.json['photo'])
        with open(os.path.join(app.config['UPLOAD_FOLDER'], "toRecognize.jpg"), 'wb') as f:
            f.write(file)
        return flask.jsonify({"letter":  recognizer.image_recognizer()}), 200
    except:
        return flask.jsonify({"STATUS": "ERROR"}), 500


'''
    Simple endpoint that recognizes gestures.
    It uses POST method and the body should be a multipart-form-data type thingy
    the request body  with only one field, video.
    Ex.
    {
      video: .mp4 file
    }
    The given back response is a JSON object with the prediction
'''

@app.route("/asl/gesture",  methods = ["POST"])
def gesture_recognizer():
    try:
        file = flask.request.files['video']
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], "toRecognize.mp4"))
        return flask.jsonify({"gesture": recognizer.video_recognizer()}), 200
    except:
        return flask.jsonify({"gesture": recognizer.video_recognizer()}), 200


app.run(host='0.0.0.0', port=5000, debug=True)