import csv
from tqdm import tqdm

with open('three-class-labels.csv') as labels:
  data = csv.reader(labels, delimiter=';')
  classes = [row[0] for row in data]
  print(classes)
  print(len(classes))
  with open('jester-v1-train.csv') as train:
    trainset = csv.reader(train, delimiter=';')
    with open('three-class-train.csv', 'w', newline='') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=';')
        for row in tqdm(trainset):
          if row[1] in classes:
              spamwriter.writerow([row[0], row[1]])

with open('three-class-labels.csv') as labels:
  data = csv.reader(labels, delimiter=';')
  classes = [row[0] for row in data]
  print(classes)
  print(len(classes))
  with open('jester-v1-validation.csv') as validation:
    valid = csv.reader(validation, delimiter=';')
    with open('three-class-validation.csv', 'w', newline='') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=';')
        for row in tqdm(valid):
          if row[1] in classes:
              spamwriter.writerow([row[0], row[1]])
