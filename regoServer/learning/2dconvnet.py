import os
import itertools
import cv2
import numpy as np
from tqdm import tqdm
import matplotlib.pyplot as plt
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import time
from sklearn.metrics import confusion_matrix
from torchvision import *
from torch.utils.data import Dataset, DataLoader

from confusionMatrix import ResnetConfusionMatrix
from net import Net
from dataLoader import ASL

# CODE BASED ON THIS TUTORIAL SERIES: https://pythonprogramming.net/introduction-deep-learning-neural-network-pytorch/

if torch.cuda.is_available():
    device = torch.device("cuda:0")
    print("Running on the GPU")
else:
    device = torch.device("cpu")
    print("Running on the CPU")

REBUILD_DATA = False
RETRAIN_MODEL = False

if REBUILD_DATA:
  asl = ASL()
  asl.make_training_data("C:\\Users\\ikecseti\\Desktop\\regoServer\\asl-alphabet\\train\\")
  asl.make_validation_data("C:\\Users\\ikecseti\\Desktop\\regoServer\\asl-alphabet\\validation\\")

loss_function = nn.CrossEntropyLoss()

if RETRAIN_MODEL:
  training_data = np.load("training_data.npy", allow_pickle=True)
  train_X = torch.Tensor([i[0] for i in training_data]).view(-1, 50, 50)
  train_X = train_X/255.0
  train_y = torch.Tensor([i[1] for i in training_data])

validation_data = np.load("validation_data.npy", allow_pickle=True)
test_X = torch.Tensor([i[0] for i in validation_data]).view(-1, 50, 50)
test_X = test_X/255.0
test_y = torch.Tensor([i[1] for i in validation_data])

def fwd_pass(net, X, y, train = False):
  if train:
    net.zero_grad()
  outputs = net(X)
  
  matches_top_1 = [torch.argmax(i) == torch.argmax(j) for i, j in zip(outputs, y)]
  acc_top_1 = matches_top_1.count(True)/len(matches_top_1)
  
  loss = loss_function(outputs, torch.max(y, 1)[1])

  if train:
    loss.backward()
    optimizer.step()
  return acc_top_1, loss, 

def test(net, size=32):
    X, y = test_X[:size], test_y[:size]
    with torch.no_grad():
      val_acc, val_loss = fwd_pass(net, X.view(-1, 1, 50, 50).to(device), y.to(device))
    return val_acc, val_loss

def train(net):
  BATCH_SIZE = 100
  EPOCHS = 25
  with open("model.log", "a" ) as f:
    for epoch in range(EPOCHS):
      for i in tqdm(range(0, len(train_X), BATCH_SIZE)):
        batch_X = train_X[i:i+BATCH_SIZE].view(-1,1,50,50).to(device)
        batch_y = train_y[i:i+BATCH_SIZE].to(device)
        acc_top_1,  loss = fwd_pass(net, batch_X, batch_y, train=True)  
      val_acc_top_1, val_loss = test(net, size = 100)
      print(f"EPOCH: {epoch} - ACC: {acc_top_1} - LOSS: {loss} - VACC: {val_acc_top_1} - VLOSS: {val_loss}")
      f.write(f"{MODEL_NAME},{round(time.time(), 3)},{round(float(acc_top_1),2)},{round(float(loss), 4)},{round(float(val_acc_top_1),2)},{round(float(val_loss), 4)}\n")

if RETRAIN_MODEL:
  MODEL_NAME = f"model-{int(time.time())}"
  net = Net().to(device)
  optimizer = optim.Adam(net.parameters(), lr = 0.001)
  print(MODEL_NAME)
  train(net)
  torch.save(net,"model.pth")

net2 = Net()
net2 = torch.load("model.pth", map_location="cuda:0")
net2.to(device)
net2.eval()

predicted = []
true = []

BATCH_SIZE = 1
for i in tqdm(range(0, len(test_X), BATCH_SIZE)):
  batch_X = test_X[i:i+BATCH_SIZE].view(-1,1,50,50).to(device)
  batch_y = test_y[i:i+BATCH_SIZE].to(device)
  with torch.no_grad():
      output = net2(batch_X)
      preds = torch.argmax(output).long()

      predicted.append(preds)
      true.append(torch.argmax(batch_y).long())

cm = ResnetConfusionMatrix(29, predicted, true)
cm.plot_confusion_matrix(("A", "B", "C", "D", "DEL", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N","NOTHING", "O", "P", "Q", "R", "S", "SPACE", "T", "U", "V", "W", "X", "Y", "Z"))
