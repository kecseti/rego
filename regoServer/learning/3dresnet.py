import os
import shutil
import glob
import pickle
import torch
import torch.nn as nn
from dataLoader import VideoFolder
from utils import *
from torchvision.transforms import *
import torchvision
from tqdm import tqdm
import time

# Code based on: https://github.com/udacity/CVND---Gesture-Recognition
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

def run():
    print("Loading training data")
    BATCH_SIZE = 10
    DATA_FOLDER = "C:\\Users\\ikecseti\\Desktop\\regoServer\\learning\\20bn-jester-v1"
    LABELS_CSV = "./20bn-jester-v1/annotations/ten-class-labels.csv"
    TRAIN_CSV = "./20bn-jester-v1/annotations/ten-class-train.csv"
    VALID_CSV = "./20bn-jester-v1/annotations/ten-class-validation.csv"
    transform = Compose([ CenterCrop(112), ToTensor(), Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]) ])
    train_data = VideoFolder(root=DATA_FOLDER, csv_file_input=TRAIN_CSV, csv_file_labels=LABELS_CSV, clip_size=16, nclips=1, step_size=2, is_val=False, transform=transform)
    train_loader = torch.utils.data.DataLoader(train_data, batch_size=BATCH_SIZE, shuffle=True, num_workers=5, pin_memory=True)

    print("Loading validation data")
    val_data = VideoFolder(root=DATA_FOLDER, csv_file_input=VALID_CSV, csv_file_labels=LABELS_CSV, clip_size=16, nclips=1, step_size=2, is_val=True, transform=transform)
    val_loader = torch.utils.data.DataLoader(val_data, batch_size=BATCH_SIZE, shuffle=False, num_workers=5, pin_memory=True)
    
    print("Creating model")
    model = torchvision.models.video.r3d_18(pretrained= True)
    for param in model.parameters():
        param.requires_grad = False
    num_ftrs = model.fc.in_features
    model.fc = nn.Linear(num_ftrs, len(train_data.classes))
    model = model.to(device)
    model.to(device)
    criterion = nn.CrossEntropyLoss().to(device)
    optimizer = torch.optim.SGD(model.parameters(), 0.001, momentum=0.9, weight_decay=0.00001)

    print("Start training")
    EPOCHS = 5
    since = time.time()
    for epoch in range(0, EPOCHS):
        train_loss, train_top1= train(train_loader, model, criterion, optimizer, epoch)
        val_loss, val_top1= validate(val_loader, model, criterion)
        print(f"EPOCH: {epoch} - ACC: {train_top1} - LOSS: {train_loss} - VACC: {val_top1} - VLOSS: {val_loss}")
    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))

    torch.save(model,"resnet3d.pth")


def train(train_loader, model, criterion, optimizer, epoch):
    losses = AverageMeter()
    top1 = AverageMeter()
    model.train()
    for i, (input, target) in enumerate(train_loader):
        input, target = input.to(device), target.to(device)
        model.zero_grad()
        output = model(input)
        loss = criterion(output, target)
        prec1 = accuracy(output.detach(), target.detach().cpu())
        losses.update(loss.item(), input.size(0))
        top1.update(prec1[0].item(), input.size(0))
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        if i % 100 == 0 and i > 0:
            print('Epoch: [{0}][{1}/{2}]\t Loss {loss.avg:.4f}\t Prec@1 {top1.avg:.3f}\t'.format(epoch, i, len(train_loader), loss=losses, top1=top1))
    return losses.avg, top1.avg

def validate(val_loader, model, criterion, class_to_idx=None):
    losses = AverageMeter()
    top1 = AverageMeter()
    model.eval()
    with torch.no_grad():
        for i, (input, target) in tqdm(enumerate(val_loader)):
            input, target = input.to(device), target.to(device)
            output = model(input)
            loss = criterion(output, target)
            prec1 = accuracy(output.detach(), target.detach().cpu())
            losses.update(loss.item(), input.size(0))
            top1.update(prec1[0].item(), input.size(0))
        return losses.avg, top1.avg

if __name__ == '__main__':
    run()