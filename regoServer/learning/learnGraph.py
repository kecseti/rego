import matplotlib.pyplot as plt
from matplotlib import style
from dataLoader import *
import numpy as np
import torch
import torchvision
from torchvision import datasets, models, transforms
import time
import os
import copy
from tqdm import tqdm
from net import Net
from confusionMatrix import ConfusionMatrix
from confusionMatrix import ResnetConfusionMatrix
from torchvision.transforms import *

if torch.cuda.is_available():
    device = torch.device("cuda:0")
    print("Running on the GPU")
else:
    device = torch.device("cpu")
    print("Running on the CPU")


style.use("ggplot")

model_name = "model-resnet183d-training"

REBUILD_TEST_DATA = False
CONVNET2D = False
RESNET2D = False
RESNET3D = False

def create_acc_loss_graph(model_name):
    contents = open("model.log", "r").read().split("\n")

    times = []
    accuracies = []
    losses = []

    val_accs = []
    val_losses = []

    for c in contents:
        if model_name in c:
            name, timestamp, acc, loss, val_acc, val_loss = c.split(",")

            times.append(float(timestamp))
            accuracies.append(float(acc))
            losses.append(float(loss))

            val_accs.append(float(val_acc))
            val_losses.append(float(val_loss))


    fig = plt.figure()

    ax1 = plt.subplot2grid((2,1), (0,0))
    ax2 = plt.subplot2grid((2,1), (1,0), sharex=ax1)


    ax1.plot(times, accuracies, label="acc")
    ax1.plot(times, val_accs, label="val_acc")
    ax1.legend(loc=2)
    ax2.plot(times,losses, label="loss")
    ax2.plot(times,val_losses, label="val_loss")
    ax2.legend(loc=2)
    plt.show()

def create_acc_loss_graph_learning(model_name):
    contents = open("model.log", "r").read().split("\n")

    times = []
    accuracies = []
    losses = []
    time = 0;

    for c in contents:
        if model_name in c:
            name, acc, loss = c.split(",")
            time = time +1 
            times.append(time)
            accuracies.append(float(acc))
            losses.append(float(loss))

    fig = plt.figure()

    ax1 = plt.subplot2grid((2,1), (0,0))
    ax2 = plt.subplot2grid((2,1), (1,0), sharex=ax1)

    ax1.plot(times, accuracies, label="acc")
    ax1.legend(loc=2)
    ax2.plot(times,losses, label="loss")
    ax2.legend(loc=2)
    plt.show()

create_acc_loss_graph(model_name)
create_acc_loss_graph_learning("model-resnet183d-learning")

if CONVNET2D:
    if REBUILD_TEST_DATA:
        dataLoader = ASL()
        dataLoader.make_testing_data("C:\\Users\\ikecseti\\Desktop\\allamvizs\\regoServer\\asl-alphabet\\test\\")

    testing_data = np.load("testing_data.npy", allow_pickle=True)
    test_X = torch.Tensor([i[0] for i in testing_data]).view(-1, 50, 50)
    test_X = test_X/255.0
    test_y = torch.Tensor([i[1] for i in testing_data])

    net2 = Net()
    net2 = torch.load("model.pth", map_location="cuda:0")
    net2.to(device)
    net2.eval()

    X, y = test_X, test_y
    with torch.no_grad():
        outputs = net2(X.view(-1, 1, 50, 50).to(device))
        
        matches_top_1 = [torch.argmax(i) == torch.argmax(j) for i, j in zip(outputs, y)]
        acc_top_1 = matches_top_1.count(True)/len(matches_top_1)
        print(acc_top_1)
        
        matches_top_5 = [torch.argmax(i) in torch.topk(j.cpu(), 5).indices for i, j in zip(outputs.cpu(), y.cpu())]
        acc_top_5 = matches_top_5.count(True)/len(matches_top_5)
        print(acc_top_5)

    cm = ConfusionMatrix(29, X, y, net2, device )
    cm.plot_confusion_matrix(("A", "B", "C", "D", "DEL", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N","NOTHING", "O", "P", "Q", "R", "S", "SPACE", "T", "U", "V", "W", "X", "Y", "Z"))

if RESNET2D:
    data_transforms = {
      'validation': transforms.Compose([
          transforms.ToTensor(),
          transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
      ]),
      'test': transforms.Compose([
          transforms.ToTensor(),
          transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
      ]),
    }

    data_dir = 'C:\\Users\\ikecseti\\Desktop\\allamvizs\\regoServer\\asl-alphabet'
    image_datasets = {x: datasets.ImageFolder(os.path.join(data_dir, x), data_transforms[x]) for x in ['validation', 'test']}
    dataloaders = {x: torch.utils.data.DataLoader(image_datasets[x], batch_size=1, shuffle=True) for x in ['validation', 'test']}
    validation_data_loader = dataloaders['validation']
    test_data_loader = dataloaders['test']

    net = models.resnet18()
    net = torch.load("resnet.pth", map_location="cuda:0")
    net.to(device)
    net.eval()

    print(len(test_data_loader))
    print(len(validation_data_loader))
    predicted = []
    true = []
    predicted_top5 = []

    for inputs, label in tqdm(test_data_loader):
        test_X = inputs.to(device)
        test_y = label.to(device)
        with torch.no_grad():
            output = net(test_X)
            _, preds = torch.max(output, 1)

            predicted.append(preds.cpu().numpy()[0])
            true.append(label.data.numpy()[0])
            if(label.data.numpy()[0] in torch.topk(output.cpu(), 5).indices):
                predicted_top5.append(True)
            else:
                predicted_top5.append(False)
            

    matches_top_1 = [i == j for i, j in zip(predicted, true)]
    acc_top_1 = matches_top_1.count(True)/len(matches_top_1)
    acc_top_5 = predicted_top5.count(True)/len(predicted_top5)
    print(acc_top_1)
    print(acc_top_5)
    print(len(predicted))
    cm = ResnetConfusionMatrix(29, predicted, true)
    cm.plot_confusion_matrix(("A", "B", "C", "D", "DEL", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N","NOTHING", "O", "P", "Q", "R", "S", "SPACE", "T", "U", "V", "W", "X", "Y", "Z"))

    predicted = []
    true = []
    for inputs, label in tqdm(validation_data_loader):
        test_X = inputs.to(device)
        test_y = label.to(device)
        with torch.no_grad():
            output = net(test_X)
            _, preds = torch.max(output, 1)

            predicted.append(preds.cpu().numpy()[0])
            true.append(label.data.numpy()[0])

    matches_top_1 = [i == j for i, j in zip(predicted, true)]
    acc_top_1 = matches_top_1.count(True)/len(matches_top_1)
    print(acc_top_1)

    cm = ResnetConfusionMatrix(29, predicted, true)
    cm.plot_confusion_matrix(("A", "B", "C", "D", "DEL", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N","NOTHING", "O", "P", "Q", "R", "S", "SPACE", "T", "U", "V", "W", "X", "Y", "Z"))

if RESNET3D:
    transform = Compose([ CenterCrop(112), ToTensor(), Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]) ])
    DATA_FOLDER = "C:\\Users\\ikecseti\\Desktop\\Tutorial-about-3D-convolutional-network\\20bn-jester-v1\\20bn-jester-v1"
    LABELS_CSV = "./20bn-jester-v1/annotations/ten-class-labels.csv"
    VALID_CSV = "./20bn-jester-v1/annotations/ten-class-validation.csv"

    val_data = VideoFolder(root=DATA_FOLDER, csv_file_input=VALID_CSV, csv_file_labels=LABELS_CSV, clip_size=16, nclips=1, step_size=2, is_val=True, transform=transform)
    validation_data_loader = torch.utils.data.DataLoader(val_data, batch_size=1, shuffle=False)

    net = models.video.r3d_18()
    net = torch.load("resnet3d.pth", map_location="cuda:0")
    net.to(device)
    net.eval()

    print(len(validation_data_loader))
    predicted = []
    true = []
    predicted_top5 = []

    for inputs, label in tqdm(validation_data_loader):
        test_X = inputs.to(device)
        test_y = label.to(device)
        with torch.no_grad():
            output = net(test_X)
            _, preds = torch.max(output, 1)

            predicted.append(preds.cpu().numpy()[0])
            true.append(label.data.numpy()[0])
            if(label.data.numpy()[0] in torch.topk(output.cpu(), 5).indices):
                predicted_top5.append(True)
            else:
                predicted_top5.append(False)
            

    matches_top_1 = [i == j for i, j in zip(predicted, true)]
    acc_top_1 = matches_top_1.count(True)/len(matches_top_1)
    acc_top_5 = predicted_top5.count(True)/len(predicted_top5)
    print(acc_top_1)
    print(acc_top_5)
    cm = ResnetConfusionMatrix(10, predicted, true)
    cm.plot_confusion_matrix(("No gesture",  "Stop Sign", "Zooming In With Two Fingers", "Zooming Out With Two Fingers", "Thumb Up", "Thumb Down", "Swiping Left", "Swiping Right", "Sliding Two Fingers Down", "Sliding Two Fingers Up"))
print("Na, béfejeztem")
    