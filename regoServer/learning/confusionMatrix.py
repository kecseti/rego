import matplotlib.pyplot as plt
import torch
import numpy as np
import itertools

class ConfusionMatrix():
  def __init__(self, nb_classes, X, y, net, device):
    self.nb_classes = nb_classes
    self.X = X.view(-1, 1, 50, 50)
    self.y = y;
    self.confusion_matrix = torch.zeros(nb_classes, nb_classes, dtype=torch.int64)
    with torch.no_grad():
      self.inputs = self.X.to(device)
      self.classes = self.y.to(device)
      self.outputs = net(self.inputs)
      for out, true in zip(self.outputs, self.classes):
        self.confusion_matrix[torch.argmax(true).long(), torch.argmax(out).long()] += 1

  def plot_confusion_matrix(self, classes, normalize=False, title='Confusion matrix', cmap=plt.cm.Blues):
    plt.imshow(self.confusion_matrix, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = self.confusion_matrix.max() / 2.
    for i, j in itertools.product(range(self.confusion_matrix.shape[0]), range(self.confusion_matrix.shape[1])):
        plt.text(j, i, format(self.confusion_matrix[i, j], fmt), horizontalalignment="center", color="white" if self.confusion_matrix[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.show()

class ResnetConfusionMatrix():
  def __init__(self, nb_classes, predicted, true):
    self.nb_classes = nb_classes
    self.confusion_matrix = torch.zeros(nb_classes, nb_classes, dtype=torch.int64)
    for out, true in zip(predicted, true):
      self.confusion_matrix[true, out] += 1

  def plot_confusion_matrix(self, classes, normalize=False, title='Confusion matrix', cmap=plt.cm.Blues):
    plt.imshow(self.confusion_matrix, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = self.confusion_matrix.max() / 2.
    for i, j in itertools.product(range(self.confusion_matrix.shape[0]), range(self.confusion_matrix.shape[1])):
        plt.text(j, i, format(self.confusion_matrix[i, j], fmt), horizontalalignment="center", color="white" if self.confusion_matrix[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.show()

