import torch
import torch.nn as nn
import torch.nn.functional as F
import cv2
import os
import numpy as np
from tqdm import tqdm
import glob
from PIL import Image
from torchvision.transforms import *
import csv
from collections import namedtuple

class ASL():
  IMG_SIZE = 50
  A = "A"
  B = "B"
  C = "C"
  D = "D"
  DEL = "del"
  E = "E"
  F = "F"
  G = "G"
  H = "H"
  I = "I"
  J = "J"
  K = "K"
  L = "L"
  M = "M"
  N = "N"
  NOTHING = "nothing"
  O = "O"
  P = "P"
  Q = "Q"
  R = "R"
  S = "S"
  SPACE = "space"
  T = "T"
  U = "U"
  V = "V"
  W = "W"
  X = "X"
  Y = "Y"
  Z = "Z"
  
  LABELS = {A: 0, B: 1, C: 2, D: 3, DEL: 4, E: 5, F: 6, G: 7, H: 8 , I: 9, J: 10, K: 11, L: 12, 
  M: 13, N: 14, NOTHING: 15, O: 16, P: 17, Q: 18, R: 19, S: 20, SPACE: 21, T: 22, U: 23, V: 24,
  W: 25, X: 26, Y: 27, Z: 28}
  training_data = []
  validation_data = []
  testing_data = []

  def make_training_data(self, src_path):
    for label in self.LABELS:
      img_path = os.path.join(src_path, label)
      print(img_path)
      for f in tqdm(os.listdir(img_path)):
        if "jpg" in f:
          try:
            path = os.path.join(img_path, f)
            img = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
            img = cv2.resize(img, (self.IMG_SIZE, self.IMG_SIZE))
            self.training_data.append([np.array(img), np.eye(29)[self.LABELS[label]]])
          
          except Exception as e:
            pass
    np.random.shuffle(self.training_data)
    np.save("training_data.npy", self.training_data)
  
  def make_validation_data(self, src_path):
      for label in self.LABELS:
        img_path = os.path.join(src_path, label)
        print(img_path)
        for f in tqdm(os.listdir(img_path)):
          if "jpg" in f:
            try:
              path = os.path.join(img_path, f)
              img = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
              img = cv2.resize(img, (self.IMG_SIZE, self.IMG_SIZE))
              self.validation_data.append([np.array(img), np.eye(29)[self.LABELS[label]]])
            
            except Exception as e:
              pass
      np.random.shuffle(self.validation_data)
      np.save("validation_data.npy", self.validation_data)

  def make_testing_data(self, src_path):
    for label in self.LABELS:
      img_path = os.path.join(src_path, label)
      print(img_path)
      for f in tqdm(os.listdir(img_path)):
        try:
          path = os.path.join(img_path, f)
          img = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
          img = cv2.resize(img, (self.IMG_SIZE, self.IMG_SIZE))
          self.testing_data.append([np.array(img), np.eye(29)[self.LABELS[label]]])
        except Exception as e:
          pass

    np.random.shuffle(self.testing_data)
    np.save("testing_data.npy", self.testing_data)

  def load_image(self, filepath):
      img = cv2.imread(filepath, cv2.IMREAD_GRAYSCALE)
      img = cv2.resize(img, (self.IMG_SIZE, self.IMG_SIZE))
      img = torch.Tensor(img).view(-1, 50, 50)
      return img/255.0








# Code based on: https://github.com/udacity/CVND---Gesture-Recognition
ListDataJpeg = namedtuple('ListDataJpeg', ['id', 'label', 'path'])

class JpegDataset(object):

    def __init__(self, csv_path_input, csv_path_labels, data_root):
        self.classes = self.read_csv_labels(csv_path_labels)
        self.classes_dict = self.get_two_way_dict(self.classes)
        self.csv_data = self.read_csv_input(csv_path_input, data_root)

    def read_csv_input(self, csv_path, data_root):
        csv_data = []
        with open(csv_path) as csvfile:
            csv_reader = csv.reader(csvfile, delimiter=';')
            for row in csv_reader:
                item = ListDataJpeg(row[0],
                                    row[1],
                                    os.path.join(data_root, row[0])
                                    )
                if row[1] in self.classes:
                    csv_data.append(item)
        return csv_data

    def read_csv_labels(self, csv_path):
        classes = []
        with open(csv_path) as csvfile:
            csv_reader = csv.reader(csvfile)
            for row in csv_reader:
                classes.append(row[0])
        return classes

    def get_two_way_dict(self, classes):
        classes_dict = {}
        for i, item in enumerate(classes):
            classes_dict[item] = i
            classes_dict[i] = item
        return classes_dict

IMG_EXTENSIONS = ['.jpg', '.JPG', '.jpeg', '.JPEG']

def default_loader(path):
    return Image.open(path).convert('RGB')


class VideoFolder(torch.utils.data.Dataset):

    def __init__(self, root, csv_file_input, csv_file_labels, clip_size,
                 nclips, step_size, is_val, transform=None,
                 loader=default_loader):
        self.dataset_object = JpegDataset(csv_file_input, csv_file_labels, root)

        self.csv_data = self.dataset_object.csv_data
        self.classes = self.dataset_object.classes
        self.classes_dict = self.dataset_object.classes_dict
        self.root = root
        self.transform = transform
        self.loader = loader

        self.clip_size = clip_size
        self.nclips = nclips
        self.step_size = step_size
        self.is_val = is_val

    def __getitem__(self, index):
        item = self.csv_data[index]
        img_paths = self.get_frame_names(item.path)

        imgs = []
        for img_path in img_paths:
            img = self.loader(img_path)
            img = self.transform(img)
            imgs.append(torch.unsqueeze(img, 0))

        target_idx = self.classes_dict[item.label]

        # format data to torch
        data = torch.cat(imgs)
        data = data.permute(1, 0, 2, 3)
        return (data, target_idx)

    def __len__(self):
        return len(self.csv_data)

    def get_frame_names(self, path):
        frame_names = []
        for ext in IMG_EXTENSIONS:
            frame_names.extend(glob.glob(os.path.join(path, "*" + ext)))
        frame_names = list(sorted(frame_names))
        num_frames = len(frame_names)

        # set number of necessary frames
        if self.nclips > -1:
            num_frames_necessary = self.clip_size * self.nclips * self.step_size
        else:
            num_frames_necessary = num_frames

        # pick frames
        offset = 0
        if num_frames_necessary > num_frames:
            # pad last frame if video is shorter than necessary
            frame_names += [frame_names[-1]] * (num_frames_necessary - num_frames)
        elif num_frames_necessary < num_frames:
            # If there are more frames, then sample starting offset
            diff = (num_frames - num_frames_necessary)
            # Temporal augmentation
            if not self.is_val:
                offset = np.random.randint(0, diff)
        frame_names = frame_names[offset:num_frames_necessary +offset:self.step_size]
        return frame_names