import React from 'react';
import Button from '@material-ui/core/Button';
import Gesture from '@material-ui/icons/AccessibilityNew';
import Hand from '@material-ui/icons/PanTool';
import { frontPanelStyle } from './styles'

export default class FrontPanel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      position:{ right:0 },
    }
  }

  handleClick = () => {
    this.setState({ position:this.state.position === undefined ?  { right:0 } : undefined })
    this.props.setActive();
  }

  render() {
    const panel = {...frontPanelStyle.button, ...this.state.position }
    return (
      <Button onClick={this.handleClick} variant='contained' color='primary' style = { panel }>
        <div style={ frontPanelStyle.container}>
          <img src="/logo.png" alt="reGo logo" width='40%' style = { frontPanelStyle.logo}/>
          <h1 style = { frontPanelStyle.title }>reGo</h1>
          {this.state.position === undefined ? <Hand style = { frontPanelStyle.icon}></Hand> : <Gesture style = { frontPanelStyle.icon}></Gesture>}
        </div>
      </Button>
    );
  }
}
