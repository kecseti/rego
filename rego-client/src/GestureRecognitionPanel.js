import React from 'react';
import { GesturePanelStyle } from './styles'
import Button from '@material-ui/core/Button';
import Webcam from "react-webcam";
import { gestureURL } from './constants'

export default class GesturePanel extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      gesture:'', 
      fileUpload: 'Drag and drop a video here or click',
      size: 0,
      recordButtonText : 'Start recording',
      mediaRecorderRef: null
    }
  }

  setRef = webcam => {
    this.webcam = webcam;
  };

  backgroundSetter = () => {
    if(this.state.size < 100){
      this.setState({ size: Number(this.state.size) + 5})
      setTimeout(this.backgroundSetter, 150)
    }else
      this.saveRecorded()
  }

  saveRecorded = () => {
    this.setState({ recordButtonText: 'Start recording', size: 0 })
    this.state.mediaRecorderRef.stop();
  }

  startRecording = () => {
    this.setState({ recordButtonText: 'Recording' })
    this.state.mediaRecorderRef = new MediaRecorder(this.webcam.stream, {mimeType: "video/webm"});
    this.state.mediaRecorderRef.addEventListener("dataavailable", ({ data }) => {
      if (data.size > 0) {
        const formData  = new FormData();
        formData.append('video', data)
        fetch(gestureURL,{
          method:'POST',
          body: formData
        }).then(response => response.json())
          .then((data) => {this.setState({gesture:data.gesture || 'Sorry, can not recognize',})})
        }
    })
    this.state.mediaRecorderRef.start()
    this.backgroundSetter()
  }

  componentDidMount() {
    document.getElementById('video').addEventListener('change', (event) => {
      this.setState({
        fileUpload: event.target.value.split( '\\' ).pop()
      })
    })
  }

  fileUpload = () => {
    const formData  = new FormData();
    formData.append('video', document.getElementById('video').files[0])
    if( document.getElementById('video').files[0] ){
      fetch(gestureURL,{
        method:'POST',
        body: formData
      }).then(response => response.json())
        .then((data) => {this.setState({gesture:data.gesture || 'Sorry, can not recognize',})})
    }
  }

  render() {
    const videoConstraints = {
      width: 1280,
      height: 720,
      facingMode: "user"
    };
    
    return (
      <div style={ GesturePanelStyle.container}>
        <h1 style = { GesturePanelStyle.title }>Hand gesture recognition</h1>
        <div style = { GesturePanelStyle.cameraContainer }>
          <Webcam audio={false}
                  ref={this.setRef}
                  width={500}
                  videoConstraints={videoConstraints}
          /> 
        </div>
        <div style={{position: 'relative'}}>
          <div style={{position: 'absolute', height: '100%', width: `${this.state.size}%`, background: '#fcba04'}}></div>
          <Button onClick = {this.startRecording} variant='outlined' color='primary' style = { GesturePanelStyle.recordingButton }>{this.state.recordButtonText}</Button>
        </div>
        <h1 style = { GesturePanelStyle.recognized }>{this.state.gesture}</h1>
        <div style = { GesturePanelStyle.dropZoneContainer }>
          <label for="video" style={GesturePanelStyle.fileUploadLabel}> {this.state.fileUpload} </label>
          <input type="file" id="video" style={GesturePanelStyle.fileUpload}/>
        </div>
       <Button onClick = {this.fileUpload} variant='outlined' color='primary' style = { GesturePanelStyle.button }>Recognize gesture</Button>
      </div>
    );
  }
}


