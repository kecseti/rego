import { colors } from './constants'

export const frontPanelStyle = {
  button:{
    position: 'absolute',
    height: '100vh',
    width:'50%',
    transition:'all ease-in-out 1s',
  },
  container:{
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    height:'100vh',
  },
  logo:{
    marginTop:'40%',
  },
  title:{
    fontWeight:700,
    fontSize:70
  },
  icon:{
    marginTop:'auto',
    fontSize:150,
    color:'rgba(0,0,0,0.5)'
  }
}

export const HandSignPanelStyle = {
  container:{
    width:'50%',
    height: '100vh',
    display: 'flex',
    flexDirection:'column',
    alignItems:'center'
  },
  title:{
    color:colors.text,
    marginTop:'5%',
    fontSize:'4rem',
  },
  cameraContainer:{
    marginTop:'3%',
    border:`1px solid ${colors.accent}`
  },
  dropZoneContainer:{
    width: '100%',
    marginTop:'auto'
  },
  button:{
    width:'100%',
    fontSize:'1.5rem',
  },
  recognized:{
    margin:'5%',
    color:colors.accent,
    fontSize:'7rem'
  }
}

export const GesturePanelStyle = {
  container:{
    width:'50%',
    height: '100vh',
    display: 'flex',
    flexDirection:'column',
    alignItems:'center',
    float:'right',
  },
  title:{
    color:colors.text,
    marginTop:'5%',
    fontSize:'4rem',
  },
  cameraContainer:{
    marginTop:'3%',
    border:`1px solid ${colors.accent}`,
  },
  dropZoneContainer:{
    width: '100%',
    height:'40%',
    marginTop:'auto'
  },
  button:{
    width:'100%',
    fontSize:'1.5rem',
  },
  recordingButton:{
    width:500,
    fontSize:'1.5rem',
  },
  recognized:{
    margin:'5%',
    color:colors.accent,
    fontSize:'4rem'
  },
  fileUploadLabel: {
    color:'#fcba04', 
    background:'rgba(0,0,0,0.1)',
    marginTop:'10%',
    width:'100%',
    height:'60%',
    borderTop:'1px dashed #fcba04',
    borderBottom:'1px dashed #fcba04', 
    fontSize:32, 
    cursor:'pointer', 
    display: 'inline-block', 
    textAlign:'center'
  },
  fileUpload: {
    color:'#fcba04', 
    background:'rgba(0,0,0,0.1)', 
    width:'50%', 
    height:'30%', 
    borderTop:'1px dashed #fcba04',
    borderBottom:'1px dashed #fcba04', 
    fontSize:32, 
    cursor:'pointer', 
    display: 'inline-block',
    position: 'absolute',
    left:'50%',
    opacity:0
  }
}