import React from 'react';
import './App.css';
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import FrontPanel from './FrontPanel';
import HandSignRecognitionPanel from './HandSignRecognitionPanel'
import GestureRecognitionPanel from './GestureRecognitionPanel'
import { colors } from './constants';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: colors.accent,
    },
    secondary: {
      main: '#f44336',
    },
  },
});

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      hand:true,
    }
  }
  setActive = () => {
    this.setState({hand:!this.state.hand})
  }
  render() {
    return (
      <ThemeProvider theme={theme}>
        <FrontPanel setActive = {this.setActive}/>
        {this.state.hand === true ? <HandSignRecognitionPanel /> : <GestureRecognitionPanel />}
      </ThemeProvider>
    );
  }
}


