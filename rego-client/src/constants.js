export const colors = {
  dark: '#2d3047',
  accent: '#fcba04',
  text: 'white',
}

export const alphabetURL = 'http://localhost:5000/asl/alphabet'
export const gestureURL =  'http://localhost:5000/asl/gesture'