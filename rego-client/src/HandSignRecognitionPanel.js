import React from 'react';
import { HandSignPanelStyle } from './styles'
import { DropzoneArea } from 'material-ui-dropzone';
import Button from '@material-ui/core/Button';
import Webcam from "react-webcam";
import { alphabetURL } from './constants'

export default class HandSignPanel extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      image: null,
      letter:' '
    }
  }
  setRef = webcam => {
    this.webcam = webcam;
  };
  
  capture = () => {
    if(this.state.image === null){
      const imageSrc = this.webcam.getScreenshot();
      this.setState({ image: imageSrc });
      console.log('From camera')
    } else {
      fetch(alphabetURL,{
          method:'POST',
          headers: {
            'Content-Type': 'application/json'
          }, 
          body: JSON.stringify({photo:this.state.image.split(',')[1]})
        }).then(response => response.json())
          .then((data) => {
            this.setState({
              image: null,
              letter:data.letter || 'Sorry, can not recognize'
            })

          })
    }
  };

  getBase64(file, cb) {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
        cb(reader.result)
    };
    reader.onerror = function (error) {
        console.log('Error: ', error);
    };
  }

  upload = async (files) => {
    if(files.length > 0){
     this.getBase64(files[0], (result) =>{
      this.setState({ image: result });
      console.log('From file')
     })
    }
  }

  render() {
    const videoConstraints = {
      width: 1280,
      height: 720,
      facingMode: "user"
    };
    return (
      <div style={ HandSignPanelStyle.container}>
        <h1 style = { HandSignPanelStyle.title }>Hand Sign recognition</h1>
        <div style = { HandSignPanelStyle.cameraContainer }>
          <Webcam audio={false}
                  ref={this.setRef}
                  screenshotFormat="image/jpeg"
                  width={500}
                  videoConstraints={videoConstraints}
          /> 
        </div>
        <h1 style = { HandSignPanelStyle.recognized }>{this.state.letter}</h1>
        <div style = { HandSignPanelStyle.dropZoneContainer }>
          <DropzoneArea acceptedFiles={['image/*']}
                        dropzoneText={"Drag and drop an image here or click"}
                        onChange={(files) => {this.upload(files)}} 
                        filesLimit = {1}
                        showAlerts = {false}
          />
        </div>
       <Button onClick={this.capture} variant='outlined' color='primary' style = { HandSignPanelStyle.button }>Recognize hand sign</Button>
      </div>
    );
  }
}


