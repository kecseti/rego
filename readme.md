# Startup
## Client Startup
In the rego-client directory run npm install/yarn.
Run npm start/yarn start.

## Server Startup
In the regoServer directory run pip install requirements.txt
Run python regoServer.py

### To create and train the neural networks: 

#### Manual alphabet
Inside the regoServer directory go to learning directory.
Download the manual alphabet dataset from: 
and place it inside the regoServer folder.
Set rebuild data and retrain model flags inside the 2dconvnet.py to TRUE to perform these actions.
Run python 2dconvnet.py/python 2dresnet.py to start training.

#### Hand gestures
Inside the regoServer directory go to learning directory.
Download the 20BN-JESTER dataset from: https://20bn.com/datasets/jester (around 22GB)
and place it inside the learning folder.
You can specify your own labels inside the 20bn-jester-v1/annotations folder by modifying the ten-class-labels.csv 
and then run python ten-class-maker.py
To train the model run python 3dresnet.py (depending on the machine but it is a time consuming task). 

To visualize the trainign and calculate confusion matrix for a model inside the learnGraph.py set the coresponding flag to TRUE then run python learnGraph.py.

