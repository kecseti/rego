\contentsline {chapter}{\numberline {1}Bevezetés}{1}{chapter.1}%
\contentsline {chapter}{\numberline {2}Elméleti áttekintés}{3}{chapter.2}%
\contentsline {section}{\numberline {2.1}Neurális hálózatok}{3}{section.2.1}%
\contentsline {section}{\numberline {2.2}Konvolúciós neurális hálózat}{6}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Háromdimenziós konvolúciós neurális hálózatok}{8}{subsection.2.2.1}%
\contentsline {chapter}{\numberline {3}Adathalmaz}{9}{chapter.3}%
\contentsline {section}{\numberline {3.1}Daktil felismeréshez használt adathalmaz}{9}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Adatgyűjtés}{10}{subsection.3.1.1}%
\contentsline {section}{\numberline {3.2}Kézjelek felismeréséhez használt adathalmaz}{11}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}20BN-Jester adathalmaz}{12}{subsection.3.2.1}%
\contentsline {chapter}{\numberline {4}A tanítás részletei}{13}{chapter.4}%
\contentsline {section}{\numberline {4.1}Felhasznált technológia}{13}{section.4.1}%
\contentsline {section}{\numberline {4.2}Használt architektúrák}{14}{section.4.2}%
\contentsline {section}{\numberline {4.3}Előfeldolgozás}{16}{section.4.3}%
\contentsline {section}{\numberline {4.4}Tanítás és hiperparaméterek}{16}{section.4.4}%
\contentsline {section}{\numberline {4.5}Hálózatok mentése}{17}{section.4.5}%
\contentsline {chapter}{\numberline {5}Eredmények}{18}{chapter.5}%
\contentsline {section}{\numberline {5.1}Daktil felismerés}{18}{section.5.1}%
\contentsline {section}{\numberline {5.2}Kézjelek felismerése}{22}{section.5.2}%
\contentsline {chapter}{\numberline {6}Szoftverrendszer bemutatása}{24}{chapter.6}%
\contentsline {section}{\numberline {6.1}Használt szoftver architektúra}{24}{section.6.1}%
\contentsline {section}{\numberline {6.2}Szerver}{25}{section.6.2}%
\contentsline {subsection}{\numberline {6.2.1}Felismerő modul}{25}{subsection.6.2.1}%
\contentsline {subsection}{\numberline {6.2.2}Előfeldolgozás és kiértékelés}{26}{subsection.6.2.2}%
\contentsline {section}{\numberline {6.3}Web-kliens}{26}{section.6.3}%
\contentsline {subsection}{\numberline {6.3.1}Használt technológiák}{26}{subsection.6.3.1}%
\contentsline {subsection}{\numberline {6.3.2}Komponensek}{27}{subsection.6.3.2}%
\contentsline {subsection}{\numberline {6.3.3}Működés}{28}{subsection.6.3.3}%
\contentsline {chapter}{\numberline {7}Következtetések és továbbfejlesztési lehetőségek}{29}{chapter.7}%
